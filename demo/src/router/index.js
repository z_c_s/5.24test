import Vue from 'vue'
import VueRouter from 'vue-router'

import Own from '../views/Own.vue'

import Home from '../views/Home.vue'
import My from '../views/My.vue'

import Detail from '../views/detail/Detail.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Own,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: Home
      },
      {
        path: 'my',
        component: My
      }
    ]
  },
  {
    path: '/detail',
    component: Detail
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
